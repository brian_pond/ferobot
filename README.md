## Ferobot

A program for managing Ferocity buffs, so I can spend more time playing.

### Usage
The following commands are available.

### Display Commands
```
fero
fero --help
```

#### Reset Targets to default from config.toml
```
fero --reset
```

#### List Targets who Want Fero, in Sort Order.
```
fero --targets
```

#### List Beastlords Casting Fero
```
fero --casters
```

### Remove a Target
```
fero --remove-target
```

### Add a Target
```
fero --add-target <name> <priority>
```

### Remove a Beastlord
```
fero --remove-caster
```

### Add a Beastlord
```
fero --add-caster
```

### Display Rotation on Command Line
```
fero --show_rotation
```

#### Share the Rotation on Discord
```
fero --share_rotation
```

#### Send a fun text message to Discord
```
fero --test_discord
```
