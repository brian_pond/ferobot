""" setup.py """
# https://setuptools.readthedocs.io/en/latest/setuptools.html#developer-s-guide

import ast
from os import geteuid as os_geteuid
import pathlib
import platform
import re
import sys

from setuptools import setup, find_packages
from setuptools.command.install import install
from setuptools.command.develop import develop


# globals
_PACKAGENAME = 'ferobot'
_MIN_PYTHON_VERSION = (3, 7, 0)  # Python 3.7.0 was released June 2018
_REQUIRES_PATH = 'package_reqs/install_requires.txt'


def get_package_version():
	""" # Retrieve the Package version from variable '__version__' in _PACKAGENAME/__init__.py
		# https://packaging.python.org/guides/single-sourcing-package-version/
	"""
	version_pattern = re.compile(r'__version__\s+=\s+(.*)')
	path_to_init = pathlib.Path(_PACKAGENAME) / '__init__.py'
	with open(path_to_init, 'rb') as fstream:
		app_version = str(ast.literal_eval(version_pattern.search(
			fstream.read().decode('utf-8')).group(1)))
	if not app_version:
		raise ValueError(f"Unable to find variable '__version__' in file {path_to_init}")
	return app_version


def make_install_requires():
	with open(_REQUIRES_PATH) as fstream:
		return fstream.read().strip().split('\n')


def make_python_requires():
	""" Workaround because of this issue: https://github.com/pypa/setuptools/issues/1633
		python_requires refers to the version of the -installed- package. Not the version of the build system.
		I want to validate both, so I wrote this short function.
	"""
	assert (sys.version_info >= _MIN_PYTHON_VERSION), \
	       "Minimum Python version is {}".format('.'.join(str(v) for v in _MIN_PYTHON_VERSION))
	return ">= " + platform.python_version()


def preinstall_sequence(_setuptools_mode):
	# Do not install package as root
	if os_geteuid() == 0:
		raise Exception("Install should be as a non-privileged user")


def postinstall_sequence(_setuptools_mode):
	pass


class InstallOverride(install):
	"""	Additional work when running in Install mode.
		https://www.anomaly.net.au/blog/running-pre-and-post-install-jobs-for-your-python-packages/
	"""
	def run(self):
		preinstall_sequence('install')
		install.run(self)
		postinstall_sequence('install')


class DevelopOverride(develop):
	"""	Additional work when running in Develop mode.
		https://www.anomaly.net.au/blog/running-pre-and-post-install-jobs-for-your-python-packages/
	"""
	def run(self):
		preinstall_sequence('develop')
		develop.run(self)
		postinstall_sequence('develop')


def get_entry_points():
	# https://amir.rachum.com/blog/2017/07/28/python-entry-points/
	ret = '''
	[console_scripts]
	ferobot=ferobot.main:main
	'''
	return ret


setup(
	name=_PACKAGENAME,  # The name pip will refer to.
	version=get_package_version(),
	packages=find_packages(),
	python_requires=make_python_requires(),
	install_requires=make_install_requires(),
	entry_points=get_entry_points(),
	include_package_data=False,
	# metadata to display on PyPI
	author='Mowgli',
	author_email='recall@martianskies.com',
	description='A program for micromanaging buffs.',
	keywords="datetime",
	license="MIT",
	platforms=["Linux"],
	# url="https://gitlab.com/brian_pond/datahenge_etl",
	zip_safe=False,
	# Additional code to run Before & After installation.
	cmdclass={'install': InstallOverride,
	          'develop': DevelopOverride}
)
