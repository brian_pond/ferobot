""" datahenge_etl/cli.py """

# PyPI
import click

# Project Modules
from ferobot.config import GlobalConfig
from ferobot import disclib

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


# Click Group:
@click.group(context_settings=CONTEXT_SETTINGS)
@click.version_option(version=GlobalConfig.get_package_version())
@click.option('--verbose', '-v', is_flag=True, default=False,
              help='Prefix to any comnmand for verbosity.')
def entry_point(verbose):
	""" FeroBot """
	if verbose:
		print("Verbose specified, but not-yet implemented by Developer.")


@entry_point.command('show-config')
def cmd_show_config():
	""" Listen to what's happening on Discord. """
	print(GlobalConfig.settings())


@entry_point.command('listen')
def cmd_listen():
	""" Listen to what's happening on Discord. """
	disclib.listen()


@entry_point.command('test-discord')
def cmd_test_discord():
	""" Test posting something on Discord """
	disclib.test_post()


@entry_point.command('reset')
def cmd_reset():
	""" Rest everything back to defaults. """


@entry_point.command('targets')
def cmd_targets():
	""" List all the Targets. """


@entry_point.command('casters')
def cmd_casters():
	""" List all the Beastlords. """


@entry_point.command('remove-target')
def cmd_remove_target(name):
	""" Remove a target from the List. """
	print(f"Removing target '{name}'")


@entry_point.command('add-target')
@click.argument('name')
def cmd_add_target(name):
	""" Remove a target from the List. """
	print(f"Adding target '{name}'")

# Casters of Buffs
@entry_point.command('remove-caster')
@click.argument('name')
def cmd_remove_caster(name):
	""" Remove a Beastlord from the raid. """
	print(f"Removing caster '{name}'")


@entry_point.command('add-caster')
@click.argument('name')
def cmd_add_caster(name):
	""" Add a Beastlord to the raid. """
	print(f"Adding caster '{name}'")


@entry_point.command('show-rotation')
def cmd_show_rotation():
	""" Display the current rotation. """


# @entry_point.command('run')
# @click.argument('command', type=click.Choice(['single', 'all'], case_sensitive=False))
# @click.option('--debug/--no-debug', default=False)
# @click.option('-t', '--table', type=str)
# def cmd_run(command, debug, table=None):
