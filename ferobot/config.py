""" config.py"""

# Standard library
from os import mkdir
import pathlib
import pprint
from sys import exit as sys_exit

import logging

# Third Party
from semantic_version import Version as SemanticVersion
import toml

from ferobot import __version__

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('config')


class GlobalConfig:
	"""Class to hold, read, and edit global configuration data."""
	__REPO_DIR = pathlib.Path(__file__).parent.parent
	__CONFIGFILE_PATH = pathlib.Path.home() / '.config/ferobot/config.toml'

	# WARNING: Do not use 'None' below, or key-value will be ommitted in the TOML file.
	__master_config_template = {
		"title": "Configuration for Ferobot",
		"casters": [ "Mowgli", "Distinguished", "Beastyy" ],
		"targets": [ { 'name': "Nirox", 'priority' : 1 },
					 { 'name': "Seev", 'priority' : 2 } ]
	}

	__config_dict = {}  # becomes a populared Dictionary once initialized.

	@staticmethod
	def get_package_version():
		""" Returns semantic version of our Python package. """
		return SemanticVersion(__version__)

	@staticmethod
	def settings():
		return GlobalConfig.__config_dict

	@staticmethod
	def get_config_filepath():
		return GlobalConfig.__CONFIGFILE_PATH

	@staticmethod
	def debug_mode_enabled():
		""" Used to decide whether to output additional text to the CLI."""
		return GlobalConfig.settings()['debug_mode']

	@staticmethod
	def print_config():
		"""Print the master configuration settings to stdout."""
		printer = pprint.PrettyPrinter(indent=4)
		printer.pprint(GlobalConfig.settings())

	@staticmethod
	def init_settings():
		""" Load from file, otherwise use the default settings."""
		try:
			GlobalConfig.__load_settings_from_file()
		except FileNotFoundError:
			GlobalConfig.revert_to_defaults()

		if not GlobalConfig.settings():
			raise Exception("Unable to initialize master configuration settings.")

		GlobalConfig.validate_config()

	@staticmethod
	def validate_config():
		if 'casters' not in GlobalConfig.settings() or 'targets' not in GlobalConfig.settings():
			msg = "Invalid configuration file: does not define '[[sources]] and [[targets]]"
			print(msg)
			sys_exit(1)

	@staticmethod
	def writeback_to_file():
		print("Writing new master configuration (from default template) to disk.")
		with open(GlobalConfig.get_config_filepath(), "w") as fstream:
			toml.dump(GlobalConfig.settings(), fstream)

	@staticmethod
	def revert_to_defaults():
		"""Revert master configuration to default setting. """
		print("Reverting to default master configuration...")
		configfile_path = GlobalConfig.get_config_filepath()
		if not configfile_path.parent.is_dir():
			mkdir(configfile_path.parent)

		# 1. Change Settings
		GlobalConfig.__config_dict = GlobalConfig.__master_config_template

		# 2. Writeback the Settings to disk.
		with open(configfile_path, "w") as fstream:
			toml.dump(GlobalConfig.settings(), fstream)

	@staticmethod
	def __load_settings_from_file():
		""" Load master settings from TOML file."""
		file_path = GlobalConfig.get_config_filepath()
		if not file_path.exists():
			msg = f"Error: Config file '{file_path}' does not exist."
			logger.debug(msg)
			raise FileNotFoundError(msg)
		GlobalConfig.__config_dict = (toml.load(file_path))

		return GlobalConfig.settings()

	@staticmethod
	def print_memory_address():
		""" Used when debugging to check for duplicate GlobalConfig objects."""
		print(f"Memory address of GlobalConfig: {id(GlobalConfig)}")


def update_global_config(key, value):
	# pylint: disable=import-outside-toplevel
	print(f"Setting master config key '{key}' to '{value}'")
	GlobalConfig.settings()[key] = value


def edit_config_file():
	""" Use the environment's default text editor to edit the configuration file."""
	import subprocess
	cmd = f"xdg-open {GlobalConfig.get_config_filepath()}"
	subprocess.run(cmd, shell=True, check=True)
