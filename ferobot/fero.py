""" fero.py """
import itertools
from ferobot.config import GlobalConfig
from ferobot import utils

# pylint: disable=blacklisted-name


class CasterTargets():
	""" An object that describes a Caster's targets. """
	def __init__(self, caster_name):
		self.__caster_name = caster_name
		self.__targets = []  # List of Tuples

	def add_target_by_name(self, target_name):
		if target_name not in Fero.target_names():
			raise ValueError("No such target named '{target_name}'")
		target = next(foo for foo in Fero.targets if foo[0] == target_name)
		self.add_target(target)

	def add_target(self, target):
		if not isinstance(target, tuple):
			raise TypeError("Argument 'target' should be a tuple.")

		if target[0] not in Fero.target_names():
			raise ValueError("No such target named '{target_name}'")
		self.__targets.append(target)

	def remove_target_by_name(self, target_name):
		if not isinstance(target_name, str):
			raise TypeError("Argument 'target_name' should be a string.")
		if target_name not in self.list_target_names():
			raise ValueError("No such target named '{target_name}'")
		target = next(foo for foo in self.__targets if foo[0] == target_name)
		self.remove_target(target)

	def remove_target(self, target):
		if not isinstance(target, tuple):
			raise TypeError("Argument 'target' should be a tuple.")
		if target not in self.__targets:
			raise ValueError(f"No such target named '{target[0]}'")
		self.__targets.remove(target)

	def find_local_target_by_name(self, target_name):
		if not isinstance(target_name, str):
			raise TypeError("Argument 'target_name' should be a string.")
		if target_name not in self.list_target_names():
			raise ValueError("No such target named '{target_name}'")
		return next(foo for foo in self.__targets if foo[0] == target_name)

	def list_target_names(self, title_case=False):
		if title_case:
			return [ foo[0].title() for foo in self.__targets ]
		return  [ foo[0] for foo in self.__targets ]

	def target_name_string(self):
		return ', '.join(self.list_target_names(True))

	def caster_name(self):
		return self.__caster_name

	def target_length(self):
		return len(self.__targets)

	def get_last_target(self):
		num_targets = len(self.__targets)
		return self.__targets[num_targets - 1]

	def sort_targets(self):
		self.__targets = sorted(self.__targets,  key=lambda tup: tup[1])


class Fero():
	""" Class Fero """
	casters = []
	targets = []		# [ (name,priority),(name,priority), ...  ]
	assignments = []   # [ CasterTargets, CasterTargets, CasterTargets]

	@staticmethod
	def target_names():
		return [ foo[0] for foo in Fero.targets ]

	@staticmethod
	def print_assignments():
		if Fero.assignments == []:
			return "No rotation has been defined (try 'fero calc')"
		num_targets = 0
		msg = "--- ROTATION ---\n"
		for caster_targets in Fero.assignments:
			msg += f"{caster_targets.caster_name().title()} : {caster_targets.target_name_string()}\n"
			num_targets += len(caster_targets.list_target_names())
		msg += "-----\n"
		msg += f"Total Buff Targets: {num_targets}"
		return msg

	@staticmethod
	def init_all():
		""" Fetch the default values from the TOML configuration file. """
		Fero.casters = [ str(foo).lower() for foo in GlobalConfig.settings()['casters'] ]
		Fero.targets = [ (str(foo['name']).lower(), foo['priority']) for foo in GlobalConfig.settings()['targets'] ]
		Fero.assignments = []  # List of CasterTargets

	@staticmethod
	def calc_assignments():
		""" This function generates Perfect Sorting, but is not always the solution to every problem. """
		if Fero.targets == [] or Fero.casters == []:
			Fero.init_all()
		# Reset assignments to empty List:
		Fero.assignments = []
		for caster in Fero.casters:
			Fero.assignments.append(CasterTargets(caster))

		# Super Cool!  An iterator that infinitely repeats the Casters, in their original sort order.
		casters = itertools.chain.from_iterable(itertools.repeat(Fero.casters))

		targets = sorted(Fero.targets, key=lambda tup: tup[1])  # Sort the List of Tuples by priority.

		for idx, foo_tuple in enumerate(targets):
			# print(f"Processing target #{idx}")
			this_caster = next(casters) # Get next caster, from an infinite, repeating list of casters.
			assignment = find_assignment_by_caster(this_caster)
			assignment.add_target(foo_tuple)

		return Fero.print_assignments()

	@staticmethod
	def remove(name):
		if name in Fero.casters:
			return Fero.__remove_caster(name)
		if name in [ foo[0] for foo in Fero.targets ]:
			return Fero.__remove_target(name)
		return f"Error: '{name}' is not a caster or target."

	@staticmethod
	def __remove_caster(name):
		if name not in Fero.casters:
			return f"Error: No such caster '{name}'"
		Fero.casters.remove(name)
		# Requires a complete rebuild
		return Fero.calc_assignments()

	@staticmethod
	def __remove_target(target_name):
		if target_name not in Fero.target_names():
			return "Error: No such target '{name}'"

		# Find the Assignment that had this Target.
		try:
			ret = find_assignment_by_target(target_name)  # (index, caster_name)
			if not ret:
				raise StopIteration
		except StopIteration:
			return f"Unable to find assignment with target named '{target_name}'"

		# Remove from master list of available targets.
		Fero.targets = [foo_tuple for foo_tuple in Fero.targets if foo_tuple[0] != target_name]
		Fero.assignments[ret[0]].remove_target_by_name(target_name)

		# Unless this leaves things wildly unbalanced, return the Assignments.
		return Fero.print_assignments()
		# return Fero.calc_assignments()

	@staticmethod
	def add_caster(name):
		if name in Fero.casters:
			return "Error: Already have a caster named '{name}'"
		Fero.casters.append(name.lower())
		# Adding a new Caster should require a Full Recalculation
		# I cannot think of another way to fairly assign Targets, based on Priority
		return Fero.calc_assignments()

	@staticmethod
	def add_target(name, priority):
		if name in [ foo[0] for foo in Fero.targets ]:
			return f"Error: Target with name '{name.title()}' already exists."

		if name in Fero.casters:
			return f"{name.title()} is a Beastlord: they can cast their own buffs!"

		new_target = (name, priority)
		Fero.targets.append(new_target)

		# Find least-utilized Caster.
		caster = Fero.__get_least_used_caster()

		# Find their list of Assignments
		assignment = next(foo for foo in Fero.assignments if foo.caster_name() == caster[0])

		# Append new target
		assignment.add_target(new_target)

		# Re-Sort
		Fero.sort_caster_targets()
		return Fero.print_assignments()

	@staticmethod
	def sort_caster_targets():
		for combo in Fero.assignments:
			combo.sort_targets()
		return Fero.calc_assignments()

	@staticmethod
	def __get_least_used_caster():
		caster_list = [ [ foo.caster_name(), foo.target_length() ] for foo in Fero.assignments]
		# Get the 'name' of the least-utilized caster
		return utils.min_of_list_by_weight(caster_list)


def find_assignment_by_caster(caster_name):
	return next(foo for foo in Fero.assignments
	            if foo.caster_name() == caster_name)


def find_assignment_by_target(target_name):
	""" Returns tuple (Index, Caster Name) """
	for idx, caster_targets in enumerate(Fero.assignments):
		if target_name in caster_targets.list_target_names():
			return (idx, caster_targets.caster_name)
	# If nothing found:
	return None
