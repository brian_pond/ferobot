""" test.py """

from ferobot import fero
from ferobot.config import GlobalConfig
GlobalConfig.init_settings()

print(fero.Fero.calc_assignments())
print(fero.Fero.remove('rodric'))
print(fero.Fero.add_caster('Shylar'))
print(fero.Fero.add_target('Rodric',1))
