""" utils.py """


def max_of_list_by_weight(some_list):
	# nested_list = [['cherry', 7], ['apple', 100], ['anaconda', 1360]]
	if not some_list:
		raise ValueError('empty sequence')

	maximum = some_list[0]

	for item in some_list:
		# Compare elements by weight stored in 2nd element.
		if item[1] > maximum[1]:
			maximum = item

	return maximum


def min_of_list_by_weight(list_of_lists):
	# nested_list = [['cherry', 7], ['apple', 100], ['anaconda', 1360]]
	if not list_of_lists:
		raise ValueError('empty sequence')

	minimum = list_of_lists[0]

	for item in list_of_lists:
		# Compare elements by weight stored in 2nd element.
		if item[1] < minimum[1]:
			minimum = item

	return minimum
