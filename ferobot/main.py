""" ferobot/main.py """
import logging

from ferobot import cli
from ferobot.config import GlobalConfig

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger('main.py')

def main():
	GlobalConfig.init_settings()  # updates the '_settings' object in MasterConfig class.
	cli.entry_point()  # pylint: disable=no-value-for-parameter


if __name__ == '__main__':
	main()
