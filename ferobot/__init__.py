""" ferobot/__init__.py """

# Global variables
__version__ = "0.0.1"  # Setuptools reads this during package installation.

if __name__ == "__main__":
	raise Exception("This top-level module should never be run as __main__.")
