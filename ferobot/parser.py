""" parser.py """

from ferobot.fero import Fero

def parse_message(author, message):
	""" Returns a string back. """
	txt = message.lower()

	if txt == 'fero dhorrin':
		return "Muhaha!  Eat shit Dhorrin! :)"

	if txt in ('fero commands', 'fero help'):
		ret = "hello fero (Say hello to fero)"
		ret += "\nfero channel (Counts members in this Channel)"
		ret += "\nfero show (Shows the buff rotation)"
		ret += "\nfero calc (Calculates the buff rotation)"
		ret += "\nfero add <name> <priority>"
		ret += "\nfero remove <player name>"
		ret += "\nrfero caster <beastlord name> (add a bst)"
		ret += "\nfero reset (Resets back to defaults)"
		ret += "\nfero dhorrin (What you'd expect, naturally.)"
		return ret

	if txt == "fero channel":
		from ferobot.disclib import get_guild
		msg = ''
		online = 0
		idle = 0
		offline = 0

		for member in get_guild().members:
			if str(member.status) == "online":
				online += 1
			if str(member.status) == "offline":
				offline += 1
			else:
				print(f"Some other status: {member.status}")
				idle += 1
		msg += f"\n```Online: {online}.\nIdle/busy/dnd: {idle}.\nOffline: {offline}```"
		return msg

	if txt == "fero show":
		return Fero.print_assignments()

	if txt == "fero calc":
		return Fero.calc_assignments()

	if txt == "fero reset":
		Fero.init_all()
		return Fero.calc_assignments()

	if txt.startswith("fero remove"):
		name = txt.replace('fero remove', '')
		name = name.strip()
		return Fero.remove(name)

	if txt.startswith("fero add"):
		name = txt.replace('fero add', '')
		name = name.strip()
		name_priority = name.split(' ')
		print(f"DEBUG: {name_priority}")
		if len(name_priority) < 2:
			return "Error. Usage: 'fero add Rageeii 3'"
		try:
			priority = int(name_priority[1])
		except Exception:
			return "Error. Priority should be a whole number (1, 2, 3, etc.)"
		return Fero.add_target(name_priority[0], priority)

	if txt.startswith("fero caster"):
		name = txt.replace('fero caster', '')
		name = name.strip()
		return Fero.add_caster(name)

	return None
