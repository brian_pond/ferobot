""" disclib.py """
import logging

# Third Party Apps
import discord

import ferobot.parser as parser
logging.basicConfig(level=logging.INFO)

# token = open("token.txt", "r").read()  # I've opted to just save my token to a text file.

class MowBot(discord.Client):
	""" Class for doing fun things with Discord. """
	tfe_guild = None
	kitty_channel = None

	async def on_ready(self):
		print("--- Begin Initialization ---")
		print(f"* Client is logged on as '{self.user}'")
		print(f"* We are connected to these guilds:\n {self.guilds}")
		await self.change_presence(activity = discord.Activity(\
			name = 'EverQuest',
			type = discord.ActivityType.watching))
		MowBot.tfe_guild = self.get_guild(550553870586216448)
		if not MowBot.tfe_guild:
			print("ERROR: No TFE Guild Found!")
			return
		MowBot.kitty_channel = MowBot.tfe_guild.get_channel(681618804052197386)
		print("--- End Initialization ---")

	# These are messages that are sent to us.
	async def on_message(self, message):

		print(f"Channel: '{message.channel}' : {message.author}: {message.author.name}: {message.content}")

		# But don't respond to ourselves
		if message.author == self.user:
			return

		msg = message.content.lower()

		if msg in [ 'hello fero', 'fero hello' ]:
			await message.channel.send(f"Hello, {message.author.display_name}")
			return

		# From here forward, only accept additional commands from kitty-channel
		#if message.channel != MowBot.kitty_channel.name:
		#	return

		ret = parser.parse_message(message.author, msg)
		if ret:
			await message.channel.send(ret)
			#await MowBot.kitty_channel.send(ret)


def listen():
	client = MowBot()
	client.run('NzczNjQyMTE2NTkyMDQxOTg0.X6MMXg.Jjx4RpApCiIy0RNeBYUsXeYHU5A')


def get_guild():
	return MowBot.tfe_guild
